# random_face_generator

Adaptation of  [DarkWallet](https://github.com/darkwallet/darkwallet) avatar face generator:
 
https://github.com/darkwallet/darkwallet/blob/develop/src/js/frontend/directives/face.js

You should to pass a 32 chars hex string like `b218b295b0af1f79d05ed36a0c54fcda` typically generated as cryptographic identifier.

![](screenshot.png)


Possible errors:

- Dart doesn't support 64 bit int numbers, so I adapt the algorithm to work different and could cause bugs. 