import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:random_face_generator/random_face_generator.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
  }

  List<String> hexIds = ["b218b295b0af1f79d05ed36a0c54fcda" , "34f978c3551576d7e8427eb5913e809d" , "e3ce975d236fadca17e381e217993449","4eb85b340bad7d54958e71634c8dfb9e"];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
            child: ListView.builder
            (
              itemCount: hexIds.length,
              itemBuilder: (BuildContext ctxt, int index) {
                return new RandomFaceGenerator(
                    hexIds[index]
                );
              }
          )

        ),
      ),
    );
  }
}
