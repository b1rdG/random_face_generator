import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:random_face_generator/random_face_generator.dart';

void main() {
  const MethodChannel channel = MethodChannel('random_face_generator');

  TestWidgetsFlutterBinding.ensureInitialized();

  test('test generate face', () async {
    List<String> hexIds = ["b218b295b0af1f79d05ed36a0c54fcda" , "34f978c3551576d7e8427eb5913e809d" , "e3ce975d236fadca17e381e217993449","4eb85b340bad7d54958e71634c8dfb9e"];

    var rfState = new RandomFaceState();
    for(String s in hexIds){
      rfState.initiate(s, RandomFaceSize.LITTLE);
    }
  });
}
