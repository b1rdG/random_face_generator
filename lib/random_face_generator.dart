import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:convert/convert.dart';

enum RandomFaceSize {LITTLE, BIG}

Map<String,List<Image>> _iconCache;

class RandomFaceGenerator extends StatefulWidget {

  final RandomFaceSize size;
  final String dataHex;

  RandomFaceGenerator(this.dataHex, {this.size = RandomFaceSize.BIG});

  @override
  RandomFaceState createState() => RandomFaceState();
}

class RandomFaceState extends State<RandomFaceGenerator>{

  String _background;
  String _clothes;
  String _face;
  String _eye;
  String _head;
  String _mouth;

  String _size;
  int _nPieces = 6;

  Map<String, List<int>> _totalPieces = {
    'female': [5, 4, 33, 17, 59, 53],
    'male': [5, 4, 36, 26, 65, 32]
  };

  List<Image> imgList;

  String _getAsset (String gender, int piece, random) {
    String head = gender == 'female' ? 'head' : 'hair';
    List<String> pieces = ['background', 'face', head, 'mouth', 'clothes', 'eye'];
    double num = (random % _totalPieces[gender][piece] + 1);
    return 'faces/$gender$_size/${pieces[piece]}${num.round()}.png';
  }

  List<dynamic> _calcDataFromFingerprint(String dataHex) {
    int females = _totalPieces['female'].reduce((previousValue, currentValue) => previousValue * currentValue);
    int males = _totalPieces['male'].reduce((previousValue, currentValue) => previousValue * currentValue);

    var ret = [];
    List<int> hexData = List.from(hex.decode(dataHex).reversed);
    double dataCache = 0;
    double tempnum = 0;
    int x = 0;
    for (int i = 0; i < hexData.length; i++) {
      if (i % 4 == 0) {
        dataCache += tempnum;
        x = 0;
      }
      tempnum += hexData[i] * ( pow(16, x*2));
      x++;
    }
    dataCache += tempnum;

    double data = dataCache  % (females+males);

    if (data % 2 == 0) {
      int i = 0;
      ret = ['female'];
      for (int i = 0; (i < _totalPieces['female'].length) && (data > 0); i++) {
        ret.add(data % _totalPieces['female'][i]);
        data = data / _totalPieces['female'][i];
      }
    } else {
      data = data - females;
      var i = 0;
      ret = ['male'];
      while (data > 0 || i < _totalPieces['male'].length) {
        ret.add(data % _totalPieces['male'][i]);
        data = data / _totalPieces['male'][i];
        i++;
      }
    }
    return ret;
  }

  List<Image> _generateImageList(List<dynamic> data){
    String gender = data[0];
    List<Image> imgList = new List<Image>();
    for (var i=0; i<_nPieces; i++) {
      imgList.add(Image.asset( _getAsset(gender, i, data[i+1]),package:'random_face_generator'));
    }
    return imgList;
  }

  List<Image> _generateAnonImage() {
    List<Image> imgList = new List<Image>();
    imgList.add(Image.asset( 'faces/anon'+_size+'.png',package:'random_face_generator'));
    return imgList;
  }

  List<Image> _createFromHex(String dataHex) {
    var iconId = dataHex + _size;
    if (_iconCache.containsKey(iconId) && _iconCache[iconId] != null) {
      return _iconCache[iconId];
    } else {
      if (dataHex.isNotEmpty) {
        _iconCache[iconId] = _generateImageList(_calcDataFromFingerprint(dataHex),);
        return _iconCache[iconId];
      } else {
        return _generateAnonImage();
      }
    }
  }

  void initiate(String dataHex, RandomFaceSize size){
    _iconCache ??= new Map<String, List<Image>>();
    _size = size == RandomFaceSize.LITTLE ? '32' : '64';
    imgList = _createFromHex(dataHex);
  }

  @override
  void initState() {
    super.initState();
    imgList = new List<Image>();
    _iconCache ??= new Map<String, List<Image>>();
    _size = widget.size == RandomFaceSize.LITTLE ? '32' : '64';
    imgList = _createFromHex(widget.dataHex);
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      imgList = _createFromHex(widget.dataHex);
    });
    return Stack(
      children: <Widget>[
        imgList[0],
        imgList[1],
        imgList[2],
        imgList[3],
        imgList[4],
        imgList[5],
      ],
    );
  }
}
